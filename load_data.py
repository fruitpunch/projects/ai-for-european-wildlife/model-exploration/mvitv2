import ast
import asyncio
import os
from pathlib import Path
import sqlite3
from trapper_client import TrapperClient, get_images
import pandas as pd
from typing import Tuple, Iterable
from PIL import Image
from tqdm import tqdm


def read_sql_query(sql_path: Path) -> str:
    return Path(sql_path).read_text()


def load_data(
    data_folder: Path,
    project_id: int,
    limit_per_class: int,
    dataset_size: int,
    batch_size: int,
) -> Tuple[Iterable[Image.Image], pd.DataFrame]:
    import ssl

    ssl._create_default_https_context = ssl._create_unverified_context

    client = TrapperClient(token=os.environ["TRAPPER_TOKEN"])

    df_observations = pd.read_csv(asyncio.run(client.get_observations(project_id)))
    df_observations["bboxes"] = df_observations["bboxes"].map(
        lambda row: ast.literal_eval(row)
        if isinstance(row, str)
        else [[None, None, None, None]]
    )
    df_observations = df_observations.explode("bboxes")
    df_observations[
        ["center_x_ratio", "center_y_ratio", "width_ratio", "height_ratio"]
    ] = pd.DataFrame(
        df_observations.pop("bboxes").to_list(), index=df_observations.index
    )

    df_image_files = pd.read_csv(asyncio.run(client.list_image_files(project_id)))

    with sqlite3.connect(data_folder / "db.sqlite3") as connection:
        df_observations.to_sql(
            "observations", connection, if_exists="replace", index=False
        )
        df_image_files.to_sql(
            "image_files", connection, if_exists="replace", index=False
        )

        raw_sql = read_sql_query(data_folder.parent / "sql" / "dataset_selection.sql")
        placeholders = {
            "limit_per_class": limit_per_class,
            "dataset_size": dataset_size,
        }

        cursor = connection.cursor()

        print("Generating the view")
        view = cursor.execute(raw_sql, placeholders)
        df = pd.DataFrame(
            view,
            columns=(
                "file_path",
                "center_x_ratio",
                "center_y_ratio",
                "width_ratio",
                "height_ratio",
                "common_name",
                "scientific_name",
            ),
        )

    connection.close()

    urls = df["file_path"].tolist()

    print("Downloading the images")
    images = asyncio.run(get_images(urls, batch_size))

    print(len(images))

    return images, df


if __name__ == "__main__":
    data_folder = Path(os.path.abspath(__file__)).parent / "data"
    limit_per_class = 20
    dataset_size = 200
    project_id = 13
    batch_size = 100

    load_data(data_folder, project_id, limit_per_class, dataset_size, batch_size)
