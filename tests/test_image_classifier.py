import pytest
from euwildlife.image_classifier import ImageClassifier


def test_classifier_embedding_num_features():
    # Create an instance of the Classifier class
    classifier = ImageClassifier(10, 1e-3)

    # Check if the 'embedding' attribute exists
    assert hasattr(classifier, "embedding")

    # Check if the 'num_features' attribute exists in the 'embedding' object
    assert hasattr(classifier.embedding, "num_features")
