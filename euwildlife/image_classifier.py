import pytorch_lightning as pl
import torch
import torchmetrics
from timm.models import MultiScaleVit, MultiScaleVitCfg
from torch import optim, nn
import torch.nn.functional as F


class ImageClassifier(pl.LightningModule):
    def __init__(self, num_classes: int, learning_rate: float, average: str, **kwargs):
        super(ImageClassifier, self).__init__()

        if learning_rate:
            self.lr = learning_rate
        else:
            self.lr = 1e-3
        self.embedding = MultiScaleVit(cfg=MultiScaleVitCfg(depths=(1, 2, 5, 2)), num_classes=0)

        self.classification_head = nn.Linear(in_features=self.embedding.num_features, out_features=num_classes)

        metrics = torchmetrics.MetricCollection([
            torchmetrics.Precision(num_classes=num_classes, task="multiclass", average=average),
            torchmetrics.Recall(num_classes=num_classes, task="multiclass", average=average),
            torchmetrics.F1Score(num_classes=num_classes, task="multiclass", average=average)
            ])

        self.train_metrics = metrics.clone(prefix="train/")
        self.val_metrics = metrics.clone(prefix="valid/")

        self.save_hyperparameters()

    def forward(self, images):
        return self.classification_head(self.embedding(images))

    def training_step(self, batch, batch_idx):
        images, targets = batch
        scores = self(images)

        loss = F.cross_entropy(scores, targets.squeeze(-1))
        predictions = torch.argmax(F.softmax(scores, dim=1), dim=1)

        train_output = self.train_metrics(predictions, targets.squeeze(-1), on_step=True, on_epoch=False) 
        # self.train_metrics(predictions, targets.squeeze(-1)) 
        self.log_dict(train_output)
        self.log("train/loss", loss)
        return loss

    def validation_step(self, batch, batch_idx):
        images, targets = batch
        scores = self(images)
        
        loss = F.cross_entropy(scores, targets.squeeze(-1))
        predictions = torch.argmax(F.softmax(scores, dim=1), dim=1)

        valid_output = self.val_metrics(predictions, targets.squeeze(-1))
        # self.val_metrics(predictions, targets.squeeze(-1))
        self.log_dict(valid_output)
        self.log("valid/loss", loss)
        return loss

    def configure_optimizers(self):
        return optim.Adam(self.parameters(), lr=self.lr)

