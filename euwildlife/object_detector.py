import random
from typing import List, OrderedDict, Tuple

import pytorch_lightning as pl
import timm
import timm.data
import torch
import torchmetrics
from timm.models import MultiScaleVit, MultiScaleVitCfg
from torch import optim
from torch.utils.data import DataLoader
from torchmetrics.detection import MeanAveragePrecision
from torchvision.models.detection import MaskRCNN
from torchvision.models.detection.faster_rcnn import FasterRCNN
from torchvision.models.detection.rpn import RegionProposalNetwork
from torchvision.ops import FeaturePyramidNetwork

from euwildlife.animals import Animals


class ObjectDetector(pl.LightningModule):
    def __init__(
        self,
        num_classes,
        learning_rate,
    ):
        super(ObjectDetector, self).__init__()
        self.lr = learning_rate

        self.mvit = MultiScaleVit(cfg=MultiScaleVitCfg(depths=(1, 2, 5, 2)), num_classes=0)

        self.rcnn = MaskRCNN(
            backbone=FeaturePyramidNetwork(
                in_channels_list=[stage.blocks[-1].mlp.fc2.out_features for stage in self.mvit.stages],
                out_channels=32,
            ),
            num_classes=num_classes,
        )

        self.precision = torchmetrics.Precision(
            task="multiclass", num_classes=num_classes
        )
        self.recall = torchmetrics.Recall(task="multiclass", num_classes=num_classes)
        self.f1 = torchmetrics.F1Score(task="multiclass", num_classes=num_classes)
        self.map = MeanAveragePrecision(box_format="xyxy", iou_type="bbox")

    def forward(self, images, batch_idx):
        return self.rcnn(self.mvit(images))

    def training_step(self, batch, batch_idx):
        images, targets = batch

        original_image_sizes: List[Tuple[int, int]] = []
        for img in images:
            val = img.shape[-2:]
            torch._assert(
                len(val) == 2,
                f"expecting the last two dimensions of the Tensor to be H and W instead got {img.shape[-2:]}",
            )
            original_image_sizes.append((val[0], val[1]))

        feature_map, feat_size = self.mvit.patch_embed(images)

        feature_maps = {}
        for index, stage in enumerate(self.mvit.stages):
            feature_map, feat_size = stage(feature_map, feat_size)
            new_dim = int(feature_map.size(1) ** 0.5)
            feature_maps[str(index)] = feature_map.reshape(feature_map.size(0), new_dim, new_dim,
                                                           feature_map.size(2))

        feature_maps = {key: torch.permute(value, (0, 3, 1, 2)) for key, value in feature_maps.items()}

        images, targets = self.rcnn.transform(images, targets)



        features = self.backbone(images.tensors)
        if isinstance(features, torch.Tensor):
            features = OrderedDict([("0", features)])
        proposals, proposal_losses = self.rpn(images, features, targets)
        detections, detector_losses = self.roi_heads(features, proposals, images.image_sizes, targets)
        detections = self.transform.postprocess(detections, images.image_sizes, original_image_sizes)  # type: ignore[operator]

        losses = {}
        losses.update(detector_losses)
        losses.update(proposal_losses)

        if torch.jit.is_scripting():
            if not self._has_warned:
                warnings.warn("RCNN always returns a (Losses, Detections) tuple in scripting")
                self._has_warned = True
            return losses, detections
        else:
            return self.eager_outputs(losses, detections)

    def validation_step(self, batch, batch_idx):
        return random.random()

    def configure_optimizers(self):
        optimizer = optim.Adam(self.parameters(), lr=self.lr)
        return optimizer

    def train_dataloader(self):
        data_config = timm.data.resolve_model_data_config(self.mvit)
        transforms = timm.data.create_transform(**data_config, is_training=False)

        # transform = transforms.Compose([transforms.ToTensor()])
        dataset = Animals(self.train_dataset_path, transform=transforms)
        dataloader = DataLoader(dataset, batch_size=4, shuffle=True)
        return dataloader

    def val_dataloader(self):
        data_config = timm.data.resolve_model_data_config(self.mvit)
        transforms = timm.data.create_transform(**data_config, is_training=False)

        # transform = transforms.Compose([transforms.ToTensor()])
        dataset = Animals(self.val_dataset_path, transform=transforms)
        dataloader = DataLoader(dataset, batch_size=4, shuffle=True)
        return dataloader

    def on_train_epoch_end(self):
        self.log("train_precision", self.precision.compute())
        self.log("train_recall", self.recall.compute())
        self.log("train_f1", self.f1.compute())

    def on_validation_epoch_end(self):
        self.log("val_precision", self.precision.compute())
        self.log("val_recall", self.recall.compute())
        self.log("val_f1", self.f1.compute())
