import os
from pathlib import Path

import pandas as pd
import PIL.features
from pyparsing import Iterable
import torch
from PIL import Image
from pydantic import BaseModel
from torch.utils.data import Dataset


class Animals(Dataset):
    # __slots__ = "images", "annotations", "transform"

    def __init__(self, images: Iterable[Image.Image], annotations: Iterable[int], transform=None):
        self.images = images
        self.annotations = annotations
        self.transform = transform

    def __len__(self):
        return len(self.images)

    def __getitem__(self, idx):
        image = self.images[idx]

        if self.transform:
            image = self.transform(image)
        _, image_width, image_height = image.shape

        target = self.annotations[idx]

        # Access the first value
        target = torch.tensor([target])

        return image, target
