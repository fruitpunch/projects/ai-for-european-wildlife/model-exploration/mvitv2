import hydra
import timm.data
import torch
import torch.nn.functional as F
from PIL import Image
from omegaconf import DictConfig

from config_validation import InferenceConfig
from euwildlife.image_classifier import ImageClassifier


@hydra.main(config_path=".", config_name="config.yaml", version_base="1.3.2")
def evaluate(cfg: DictConfig) -> None:
    config = InferenceConfig(
        test_directory=cfg.inference.test_directory,
        model_path=cfg.inference.model_path
    )
    checkpoint = torch.load(config.model_path)

    state_dict = {
        "embedding": {".".join(key.split(".")[1:]): value for key, value in checkpoint["state_dict"].items() if
                      key.startswith("embedding")},
        "classification_head": {".".join(key.split(".")[1:]): value for key, value in checkpoint["state_dict"].items()
                                if key.startswith("classification_head")}
    }
    model = ImageClassifier(**checkpoint["hyper_parameters"])
    model.embedding.load_state_dict(state_dict["embedding"])
    model.classification_head.load_state_dict(state_dict["classification_head"])

    model.eval()

    # Get model specific transforms (normalization, resize)
    data_config = timm.data.resolve_model_data_config(model)
    transforms = timm.data.create_transform(**data_config, is_training=False)

    for image_path in config.test_directory.glob("*"):
        img = Image.open(image_path)
        output = torch.argmax(F.softmax(model(transforms(img).unsqueeze(0))), dim=1)
        print(output)


if __name__ == "__main__":
    evaluate()
