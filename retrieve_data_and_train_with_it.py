from pathlib import Path

import yaml

from fruitpunch.helper.Annotation import materialize_data_yaml


def materialize_data_yaml(dataset_params, path_for_project, type):
    """
    take links from the train.txt, valid.txt or test.txt and copy the images to a folder
    """
    dataset_params_txt = dataset_params[type]

    images_new_path = path_for_project.joinpath(type).joinpath("images")
    labels_new_path = path_for_project.joinpath(type).joinpath("labels")
    images_new_path.mkdir(exist_ok=True, parents=True)
    labels_new_path.mkdir(exist_ok=True, parents=True)

    with open(path_for_project.joinpath(dataset_params_txt), "r") as fp:
        for i, line in enumerate(fp):
            image_path = Path(line.rstrip())

            shutil.copy(image_path, images_new_path)

            label_path = image_path.with_suffix(".txt")
            shutil.copy(label_path, labels_new_path)

def get_data():

    ## TODO change this so it works automatically everywhere
    project_id = "13"
    path_for_project = Path(f"/home/christian/work/fruitpunch/ml-pipeline/tests_data_{project_id}")

    BATCH_SIZE = 5

    dataset_params = yaml.safe_load(open(path_for_project.joinpath("k_folds/fold_0/data.yaml"), 'r'))


    materialize_data_yaml(dataset_params, path_for_project.joinpath("k_folds/fold_0/"), "train")
    materialize_data_yaml(dataset_params, path_for_project.joinpath("k_folds/fold_0/"), "val")
    # TODO do this with the test set

    dataset_params = {
        'data_dir': path_for_project.joinpath("k_folds/fold_0/"),
        'train_images_dir': 'train/images',
        'train_labels_dir': 'train/labels',
        'val_images_dir': 'val/images',
        'val_labels_dir': 'val/labels',
        'classes': dataset_params["names"]
    }


    from IPython.display import clear_output

    train_data = coco_detection_yolo_format_train(
        dataset_params={
            'data_dir': dataset_params['data_dir'],
            'images_dir': dataset_params['train_images_dir'],
            'labels_dir': dataset_params['train_labels_dir'],
            'classes': dataset_params['classes']
        },
        dataloader_params={
            'batch_size': BATCH_SIZE,
            'num_workers': 8
        }
    )

    val_data = coco_detection_yolo_format_val(
        dataset_params={
            'data_dir': dataset_params['data_dir'],
            'images_dir': dataset_params['val_images_dir'],
            'labels_dir': dataset_params['val_labels_dir'],
            'classes': dataset_params['classes']
        },
        dataloader_params={
            'batch_size': BATCH_SIZE,
            'num_workers': 2
        }
    )


if __name__ == "__main__":
    get_data()