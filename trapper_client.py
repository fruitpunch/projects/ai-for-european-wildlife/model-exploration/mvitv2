import asyncio
from io import StringIO
import os
from typing import Iterable
from urllib.request import urlopen
from PIL import Image
import requests
import pandas as pd


def http_get_sync(url: str, token: str) -> StringIO:
    headers = {"Authorization": f"Token {token}"}
    return StringIO(requests.get(url, headers=headers).text)


async def http_get(url: str, token: str) -> StringIO :
    return await asyncio.to_thread(http_get_sync, url, token)

class TrapperClient:
    def __init__(self, token: str):
        self.BASE_URL = "https://sweden.trapper-project.org"
        self.__token = token
        self.MEDIA_ENDPOINT = "media_classification/api"
        self.OBSERVATIONS_ENDPOINT = f"{self.MEDIA_ENDPOINT}/classifications/results"
        self.PROJECTS_ENDPOINT = f"{self.MEDIA_ENDPOINT}/projects"
        self.IMAGE_FILES_ENDPOINT = f"{self.MEDIA_ENDPOINT}/media"
        
    async def get_observations(self, project_id: int) -> StringIO:
        url = f'{self.BASE_URL}/{self.OBSERVATIONS_ENDPOINT}/{project_id}'
        return await http_get(url, self.__token)
    
    def list_projects(self) -> dict:
        url = f'{self.BASE_URL}/{self.PROJECTS_ENDPOINT}'
        return requests.get(url, headers={"Authorization": f"Token {self.__token}"}).json()
    
    async def list_image_files(self, project_id: int) -> StringIO:
        url = f'{self.BASE_URL}/{self.IMAGE_FILES_ENDPOINT}/{project_id}'
        return await http_get(url, self.__token)

def get_image_sync(url: str):
    return Image.open(urlopen(url)).convert('RGB')

async def get_image(url: str):
    return await asyncio.to_thread(get_image_sync, url)

async def get_images(image_urls: Iterable[str], concurrency_limit: int):
    semaphore = asyncio.Semaphore(concurrency_limit)
    images = []
    for url in image_urls:
        async with semaphore:
            image = get_image(url)
            images.append(image)
    return await asyncio.gather(*images)
