"""Training script for PyTorch image classification module.

Author: Mahmoud KOBBI <mahmoud.kobbi@gmail.com>
"""
import datetime
import os
from pathlib import Path

import hydra
import pytorch_lightning as pl
import timm.data
import torch
from omegaconf import DictConfig
from pytorch_lightning.callbacks import LearningRateMonitor, ModelCheckpoint
from pytorch_lightning.loggers import TensorBoardLogger, WandbLogger
from torch.utils.data import DataLoader, random_split

from config_validation import (
    DataLoaderConfig,
    DatasetConfig,
    ModelConfig,
    TrainingConfig,
)
from euwildlife.animals import Animals
from euwildlife.image_classifier import ImageClassifier
from load_data import load_data
from sklearn.preprocessing import LabelEncoder


@hydra.main(config_path=".", config_name="config.yaml", version_base="1.3.2")
def main(cfg: DictConfig) -> None:
    """Main Training function. Loads configuration, datasets and embedding, then trains the classifier.

    Args:
        cfg (DictConfig): Training configuration
    """
    config = TrainingConfig(
        model=ModelConfig(**cfg.model),
        train_data=DataLoaderConfig(**cfg.train_dataloader),
        val_data=DataLoaderConfig(**cfg.val_dataloader),
        dataset=DatasetConfig(**cfg.trapper),
    )

    data_folder = Path(os.path.abspath(__file__)).parent / "data"
    data_folder.mkdir(parents=True, exist_ok=True)

    images, df = load_data(
        data_folder,
        config.dataset.project_id,
        config.dataset.limit_per_class,
        config.dataset.dataset_size,
        config.dataset.batch_size,
    )

    embedding = timm.create_model(config.model.name, pretrained=True, num_classes=0)

    data_config = timm.data.resolve_model_data_config(embedding)
    transforms = timm.data.create_transform(**data_config, is_training=False)

    le = LabelEncoder()
    df['encoded_name'] = le.fit_transform(df['scientific_name'].tolist())

    # Assuming you have a dataset called 'dataset' with 1000 samples
    dataset = Animals(
        images, tuple(df["encoded_name"].tolist()), transform=transforms
    )

    num_classes = df["scientific_name"].unique().shape[0]

    # Define the split ratios (e.g., 80% for training, 20% for testing)
    train_ratio = 0.8
    val_ratio = 0.2

    # Calculate the number of samples for each split
    num_samples = len(dataset)
    num_train_samples = int(train_ratio * num_samples)
    num_test_samples = num_samples - num_train_samples

    # Use random_split to split the dataset
    train_dataset, val_dataset = random_split(
        dataset, [num_train_samples, num_test_samples]
    )

    train_dataloader = DataLoader(
        train_dataset,
        batch_size=config.train_data.batch_size,
        num_workers=config.val_data.num_workers,
        shuffle=True,
    )

    val_dataloader = DataLoader(
        val_dataset,
        batch_size=config.val_data.batch_size,
        num_workers=config.val_data.num_workers,
        shuffle=False,
    )

    model = ImageClassifier(
        num_classes=num_classes,
        learning_rate=config.model.lr,
        average="micro",
    )
    model.embedding = embedding
    if config.model.freeze_embedding:
        model.embedding.requires_grad_(False)

    checkpoint_callback = ModelCheckpoint(
        dirpath=Path(os.path.abspath(__file__)).parent / "artifacts",
        save_top_k=1,
        verbose=True,
        monitor="valid/loss",
        mode="min",
    )
    lr_logger = LearningRateMonitor()
    tensorboard_logger = TensorBoardLogger(
        "tb_logs", name="euwildlife-mvitv2-classification"
    )
    wandb_logger = WandbLogger()

    trainer = pl.Trainer(
        max_epochs=config.model.epochs,
        callbacks=[lr_logger, checkpoint_callback],
        logger=[tensorboard_logger, wandb_logger],
    )

    trainer.fit(model, train_dataloader, val_dataloader)

    current_datetime = datetime.datetime.utcnow()
    formatted_datetime = current_datetime.strftime("%Y%m%d-%H%M%S")

    torch.save(
        model.state_dict(),
        Path(os.path.abspath(__file__)).parent
        / "artifacts"
        / f"mvitv2_classifier-0.1.0-build-{formatted_datetime}.pt",
    )


if __name__ == "__main__":
    main()  # pylint: disable=no-value-for-parameter
