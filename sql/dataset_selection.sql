SELECT
            image_files.filePath,
            observations.center_x_ratio,
            observations.center_y_ratio,
            observations.width_ratio,
            observations.height_ratio,
            observations.commonName,
            observations.scientificName
        FROM
            (SELECT DISTINCT mediaID FROM observations) AS filtered_observations
        JOIN
            image_files ON image_files.mediaID = filtered_observations.mediaID
        JOIN
            observations ON observations.mediaID = filtered_observations.mediaID
        WHERE
            observations.scientificName IN (
                SELECT scientificName
                FROM observations
                where center_x_ratio is not null
                GROUP BY scientificName
                HAVING COUNT(*) >= :limit_per_class
            )
            
        LIMIT :dataset_size;