import multiprocessing
from pathlib import Path
from typing import Optional, Tuple, Iterable

from pydantic import (
    BaseModel,
    PositiveInt,
    PositiveFloat,
    AnyHttpUrl,
    validator,
    conint,
)


class ModelConfig(BaseModel):
    name: str
    epochs: PositiveInt
    lr: PositiveFloat
    num_classes: Optional[PositiveInt]
    freeze_embedding: bool

class DatasetConfig(BaseModel):
    location: Path
    project_id: int
    dataset_size: int
    batch_size: int
    limit_per_class: int

class DataLoaderConfig(BaseModel):
    batch_size: PositiveInt
    num_workers: conint(ge=1, le=multiprocessing.cpu_count())

class TrainingConfig(BaseModel):
    model: ModelConfig
    dataset: DatasetConfig
    train_data: DataLoaderConfig
    val_data: DataLoaderConfig


class InferenceConfig(BaseModel):
    model_path: Path
    test_directory: Path