# MViTv2 model

The Animal Classification project utilizes the [MViTv2 architecture](https://arxiv.org/pdf/2112.01526.pdf) implemented in PyTorch to classify animals based on input images. This documentation provides an overview of the project, installation instructions, usage guidelines, and other relevant information.

## Table of Contents

1. [Installation](#installation)
2. [Usage](#usage)
3. [Training](#training)
3. [Configuration](#configuration)
4. [File Structure](#file-structure)
5. [Contributing Guidelines](#contributing-guidelines)
6. [License](#license)
7. [Contact Information](#contact-information)

## Installation
To install and set up the Animal Classification project, follow these steps:

1. Clone the repository from GitHub:
```bash
git clone https://gitlab.com/fruitpunch/projects/ai-for-european-wildlife/model-exploration/mvitv2.git
```
2. Install the required dependencies. Ensure that you have PyTorch and other necessary libraries installed. You can use the following command to install the dependencies:

```bash
poetry install
```
3. Download the dataset for animal classification and place it in the appropriate directory. You can find more details in the [Configuration](#configuration) section.
## Usage
To use the Animal Classification project for ML inference, follow these steps:

1. Open the `config.yaml` file located in the root directory.
2. Update the inference configuration parameters according to your requirements. This includes specifying the path to the trained model checkpoint, input image size, and any other relevant settings.
Save the changes to config.yaml.
3. Open a terminal or command prompt and navigate to the project directory.
4. Run the evaluation script using Poetry:
```bash
poetry run evaluate.py
```
This will execute the evaluation script and perform classification on the specified input images using the configured model.

**Note**: Ensure that you have the necessary dependencies installed by running `poetry install`
before executing the script.
5. View the output or generated results as per the script's implementation. This may include displaying the predicted animal labels or saving the results to a file.

Feel free to modify the `config.yaml` file and the evaluate.py script to suit your specific needs and evaluation requirements.

## Training

To train the Animal Classification model, follow these steps:

1. Prepare your training data in the following format:
   - Each training image should be named as imageX.jpg, where X is a unique identifier for the image.
   - Corresponding to each image, create a text file named imageX.txt in the same directory.
   - The text file should follow the "Trapper" standard and contain the bounding box annotations for each object in the image. Each line represents an object, and the format of each line should be as follows: `class_id center_x_ratio center_y_ratio width_ratio height_ratio`
     - `class_id`: The identifier or label for the object class.
     - `center_x_ratio`: The x-coordinate of the object's center relative to the image width.
     - `center_y_ratio`: The y-coordinate of the object's center relative to the image height.
     - `width_ratio`: The width of the object relative to the image width.
     - `height_ratio`: The height of the object relative to the image height.

   Here's an example of a sample text file entry for an object with class ID 1, centered at (0.5, 0.4), and with a width and height of 0.2 and 0.3, respectively: `1 0.5 0.4 0.2 0.3`
2. Place all the training and validation images (`imageX.jpg`) and their corresponding annotation text files (`imageX.txt`) in a designated training data directory.
3. Configure the training and validation parameters in the `config.yaml file, including the paths to the training data directory, batch size, learning rate, and other hyperparameters as required.
4. Open a terminal or command prompt and navigate to the project directory.
5. Run the training script using Poetry:
   ```bash
   poetry run python train_classifier.py
   ```
   This will start the training and validation process using the specified training data and configuration.
6. Monitor the training and validation progress, including the loss and accuracy metrics, as the main script executes.
7. Once the training is complete, the trained model and any associated artifacts or checkpoints will be saved according to the specified configuration.
Feel free to modify the training data format, annotation standard, or any other training parameters to suit your specific needs.

## Configuration
The Animal Classification project supports configuration using [Hydra](https://hydra.cc). You can customize various aspects of the project's behavior using configuration file `config.yaml`. 

```yaml
# Example configuration options in config.yaml
model:
  name: 'mvitv2_tiny.fb_in1k'
  epochs: 10
  num_classes: 10
  lr: 0.001

train_data:
  directory: "/path/to/train/data/"
  batch_size: 4

val_data:
  directory: "/path/to/val/data/"
  batch_size: 4
```

## File Structure
```bash
mvitv2/
  |- artifacts/               # Directory for storing model checkpoints
  |- euwildlife/              # Source code directory
  |   |- __init.py__          
  |   |- animals.py           # Dataset class
  |   |- image_classifier.py  # Image classifier implementation
  |   |- object_detector.py   # Object detector implementation
  |- tests/                   # Directory for tests
  |- train_classifier.py                  # Training script
  |- evaluate.py              # Inference script
  |- config.yaml              # Configuration file
  |- config_validation.py     # Configuration management and validation file
  |- pyproject.toml           # Poetry project configuration file
  |- poetry.lock              # Poetry lock file
  |- README.md                # Documentation file (you are here)

```

## Contributing Guidelines
We welcome contributions to the Animal Classification project! If you'd like to contribute, please follow these guidelines:

1. Clone the repository and create your branch
```bash
git clone https://gitlab.com/fruitpunch/projects/ai-for-european-wildlife/model-exploration/mvitv2.git
cd mvitv2
git checkout -b my-feature
```
2. Install the development requirements using Poetry

```bash
poetry install --dev
```

This will install the necessary dependencies for both the project and development.

3. Pick up an issue or feature to work on. You can find a list of open issues or feature requests [here](https://gitlab.com/fruitpunch/projects/ai-for-european-wildlife/model-exploration/mvitv2/-/issues).

4. Implement your changes or add new features. Ensure that your code follows the project's coding style and best practices, which adhere to the [Google Python Style Guide](#https://google.github.io/styleguide/pyguide.html):

    - Use appropriate variable and function names.
    - Format your code, including comments, according to the Google coding style.
    - Maintain consistent indentation (4 spaces) and line lengths (max 80 characters).

5. Format your code using Black, a Python code formatter

```bash
poetry run black .
```
This ensures a consistent code style across the project.

6. Run pylint, a Python static code analysis tool, to check for any linting issues:

```bash
poetry run pylint train_classifier.py
```
Address any reported issues before proceeding.

7. Write tests to ensure the correctness of your code. Run the tests locally to verify that everything is working as expected

```bash
poetry run pytest
```

8. Commit your changes and push them
```bash
git add .
git commit -m "Add my feature"
git push origin my-feature
```

9. Create a merge request on the main repository. Provide a clear description of your changes and their purpose. Reference the relevant issue or feature request, if applicable.
We will review your merge request and provide feedback. Once approved, your changes will be merged into the main repository.

We appreciate your contributions!

## License

## Contact Information
If you have any questions, suggestions, or feedback, you can reach out to us at:
- Mahmoud Kobbi <mahmoud.kobbi@gmail.com>
- Agnethe Seim Olsen <olsenas@cardiff.ac.uk>